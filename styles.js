const React = require('react-native');
const {StyleSheet} = React;

export default {
  container:{
    flex: 1,
  },
  btnText:{
    fontSize: 30,
    },
  result: {
    flex: 2,
    backgroundColor: '#53504E',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  resultText: {
    fontSize: 30,
    color: 'white',
  },
  btn: {
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  btnText: {
     fontSize: 35,
     color: 'white',
   },
   calculationText: {
     fontSize: 42,
     color: 'white'
   },
  calculation:{
    flex: 1,
    backgroundColor: '#53504E',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  buttons: {
    flex: 7,
    flexDirection: 'row'
  },
  numbers:{
    flex: 3,
    backgroundColor: 'grey',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  operations: {
    flex: 1,
    backgroundColor: '#DE7920',
    justifyContent: 'space-around',
    alignItems: 'stretch',
    color: 'white',
  },
  landscapeOperations: {
    flex: 3,
    backgroundColor: '#DE7920',
    flexDirection: 'row',
    flexWrap: 'wrap',

  },
}


import React, {Component, useEffect} from 'react';
import styles from './styles';
import NumbersTouchableOpacityKeyboard from './NumbersTouchableOpacityKeyboard';
import OperationsTouchableOpacityKeyboard from './OperationsTouchableOpacityKeyboard';
import SplashScreen from 'react-native-splash-screen';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const portraitNumbers = [
  {content: '1', color: 'grey', width: '33.33%', height: '25%'},
  {content: '2',color: 'grey', width: '33.33%', height: '25%'},
  {content: '3',color: 'grey', width: '33.33%', height: '25%'},
  {content: '4', color: 'grey', width: '33.33%', height: '25%'},
  {content: '5',color: 'grey', width: '33.33%', height: '25%'},
  {content: '6',color: 'grey', width: '33.33%', height: '25%'},
  {content: '7', color: 'grey', width: '33.33%', height: '25%'},
  {content: '8',color: 'grey', width: '33.33%', height: '25%'},
  {content: '9',color: 'grey', width: '33.33%', height: '25%'},
  {content: '.', color: 'grey', width: '33.33%', height: '25%'},
  {content: '0',color: 'grey', width: '33.33%', height: '25%'},
  {content: '=',color: 'grey', width: '33.33%', height: '25%'}
];
const portraitOperations = [
  { content: 'AC', color: '#DE7920', width: '100%', height: '20%'},
  { content: '+', color: '#DE7920', width: '100%', height: '20%'},
  { content: '-', width: '25%', color: '#DE7920', width: '100%', height: '20%'},
  { content: '*', width: '25%', color: '#DE7920', width: '100%', height: '20%'},
  {content: '/', color: '#DE7920', width: '100%', height: '20%'}
];
const landscapeOperations = [
  {content: '2√x', color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'x!',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'e^x',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: '10^x',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'ln', color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'log10',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'e',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'x^2', color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'pi',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: 'x^3',color: '#DE7920', width: '33.33%', height: '25%'},
  {content: '+/-', color: '#DE7920', width: '33.33%', height: '25%'},
  {content: '%',color: '#DE7920', width: '33.33%', height: '25%'}
];

var splash = require('./android/app/src/main/res/drawable/splash_screen.png')
export default class App extends Component{
  componentMount(){
    SplashScreen.hide();
  }

  constructor(){
    super()
    this.operations = ['AC', '+', '-', '*', '/']

    const isPortrait = () =>{
      const dim = Dimensions.get('window');
      return dim.height > dim.width;
    }

    this.state = {
      resultText: '',
      calculationText: '',
      orientation: isPortrait() ? 'PORTRAIT' : 'LANDSCAPE'
    };

    Dimensions.addEventListener('change', () => {
      this.setState({
        orientation: isPortrait() ? 'PORTRAIT' : 'LANDSCAPE'
        });
      });
    }

    calculateResult(){
      const text = this.state.resultText
      console.log(text, eval(text));
      this.setState({
        calculationText: eval(text)
        })
    }

    buttonPressed(text){
      if(text == '='){
        return this.validate() && this.calculateResult()
      }
      this.setState({
        resultText: this.state.resultText + text
        })
    }

    validate(){
      const text = this.state.resultText
      if(text === ""){
        this.setState({
          calculationText: ""
          })
        return false
      }
      if(text.includes('!') || text.includes('^')){
        return false
      }
      if(text.includes('(')){
        if(text.includes(')')){
          return true
        }
      return false
      }
      switch (text.slice(-1)){
        case '+':
        case '-':
        case '*':
        case '/':
        case '(':
          return false
        break;
      }
      switch (text.charAt(0)){
        case '+':
        case '-':
        case '*':
        case '/':
        case 'l':
        case '√':
          return false
        break;
      }
      return true
    }

    factorial(number){
      if(number < 20){
        if(number == 0 || number == 1){
          return 1
        }
        else{
          let fact = 1;
          while(number > 1){
            fact = fact * number
            number -= 1
          }
          return fact
        }
      }
    }

    operate(operation){
      switch (operation){
        case 'AC':
          let text = this.state.resultText.split('')
          text.pop()
          this.setState({
            resultText: " ",
            calculationText: " "
            })
          break;
        case '+':
        case '-':
        case '*':
        case '/':
          const lastChar = this.state.resultText.split('').pop();
          if(this.state.text == "" || this.operations.indexOf(lastChar) > 0){
            return;
          }
          this.setState({
            resultText: this.state.resultText + operation
            })
          break;
      }
    }

    operationLandscape(operation){
    const text = this.state.resultText
    if(operation === 'e'){
      this.setState({
        resultText: "" + Math.E + "",
        calculationText: "e"
      })
    }
    else if(operation === "pi"){
      this.setState({
        resultText: "" + Math.PI + "",
        calculationText: "pi"
      })
    }
    else if(this.validate()){
      let ev = 0;
      switch (operation) {
        case '2√x':
          ev = eval(text);
          this.setState({
              resultText: "" + Math.sqrt(ev) + "",
              calculationText: "√" + ev
          });
        break;
        case 'x!':
          ev = eval(text);
          this.setState({
            resultText: "" + this.factorial(ev) + "",
            calculationText: ev + "!"
          })
        break;
        case 'e^x':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.pow(Math.E,ev) + "",
            calculationText: "e^" + ev
          })
        break;
        case '10^x':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.pow(10,ev) + "",
            calculationText: "10^" + ev
          })
        break;
        case 'ln':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.log(ev) + "",
            calculationText: "ln" + ev
          })
        break;
        case 'log10':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.log10(ev) + "",
            calculationText: "log10(" + ev + ')'
          })
        break;
        case 'x^2':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.pow(ev,2) + "",
            calculationText: ev + '^2'
          })
        break;
        case 'x^3':
          ev = eval(text);
          this.setState({
            resultText: "" + Math.pow(ev,3) + "",
            calculationText: ev + '^3'
          })
        break;
        case '+/-':
          ev = eval(text);
          this.setState({
            resultText: "-(" + ev + ")"
          })
        break;
        case '%':
          ev = eval(text);
          this.setState({
            resultText: "" + ev/100 + ""
          })
        break;
      }
    }
  }


  render(){
       return (
           <View style={styles.container}>
             <View style={styles.result}>
               <Text style={styles.resultText}>{this.state.resultText}</Text>
             </View>
             <View style={styles.calculation}>
               <Text style={styles.calculationText}>{this.state.calculationText}</Text>
             </View>
             <View style={styles.buttons}>
               {this.state.orientation == 'LANDSCAPE' ?
                   <View style={styles.landscapeOperations}>
                     {
                       landscapeOperations.map((el) => {
                                       return (
                                           <NumbersTouchableOpacityKeyboard key={el.content} content={el.content} color={el.color} width={el.width} height={el.height}
                                                                onButtonPress={this.operationLandscape.bind(this, el.content)}/>
                                       );
                                   })
                     }
                   </View> : null
                 }
               <View style={styles.numbers}>
                 {
                   portraitNumbers.map((el) => {
                                   return (
                                       <NumbersTouchableOpacityKeyboard key={el.content} content={el.content} color={el.color} width={el.width} height={el.height}
                                                            onButtonPress={this.buttonPressed.bind(this, el.content)}/>
                                   );
                               })
                 }
               </View>
               <View style={styles.operations}>
                 {
                   portraitOperations.map((el) => {
                          return (
                                <OperationsTouchableOpacityKeyboard key={el.content} content={el.content} color={el.color} width={el.width}
                                                            onButtonPress={this.operate.bind(this, el.content)}/>
                                );
                               })
                 }
               </View>
             </View>
           </View>
         );
   }
 }

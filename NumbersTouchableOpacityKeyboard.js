import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles'

class NumbersTouchableOpacityKeyboard extends Component{

  static propTypes = {
    content: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    width: PropTypes.string.isRequired,
    height: PropTypes.string.isRequired,
  }

  render(){
    const {color, content, width, height, onButtonPress} = this.props;

    return(
      <TouchableOpacity style={[styles.btn,{
        width: width,
        height: height,
        backgroundColor: color,
      }]} onPress={()=> onButtonPress({content})}>
      <Text style={styles.btnText}>{content}</Text>
      </TouchableOpacity>
    );
  }
}

export default NumbersTouchableOpacityKeyboard;
